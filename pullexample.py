#!/usr/bin/python3

from pull import *;

def pusherFunction():
	push("print", "This gets pulled");
	print("This doesn't though, be careful");
	push("invalid", "This goes nowhere");
	push("exitprint", "The puller makes this act like an exception");
	push("print", "So this won't be called");

def callerFunction():
	with Puller("print", lambda x: print("!!" + x + "!!")):
		pusherFunction();

def mainFunction():
	with Puller("exitprint", lambda x: print("!!!" + x + "!!!"), True):
		callerFunction();

mainFunction();