pullers = [];

class ExitPuller(Exception):
	def __init__(self, puller):
		self.puller = puller;

class Pushed(Exception):
	def __init__(self, type, value):
		self.type = type;
		self.value = value;
	
	def __str__(self):
		return str(self.__dict__);

class Puller:
	def __init__(self, type = "error", callback = lambda x: x, exit = False):
		self.type = type;
		self.callback = callback;
		self.exit = exit;
		
	def __enter__(self):
		pullers.insert(0, self);
		return self;
	
	def __exit__(self, type, value, traceback):
		pullers.remove(self);
		if isinstance(value, ExitPuller):
			return self == value.puller;
		return False;

	def __call__(self, pushed):
		self.callback(pushed);
		return self.exit;

def push(type, value):
	for puller in pullers:
		if puller.type == type:
			if puller(value):
				raise ExitPuller(puller);
			break;